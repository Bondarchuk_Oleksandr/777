import java.util.ArrayList;
import java.util.List;

public class Generics {
    public static void main(String[] args) {
        List<String> animals = new ArrayList<>();
        animals.add("cat"); // 0
        animals.add("dog"); // 1
        animals.add("frog"); // 2

        String animal2 = animals.get(1);
        System.out.println(animal2);
    }
}
